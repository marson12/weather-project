from matplotlib.pyplot import plot, show, title, xlabel, ylabel, legend, axis, savefig
import csv
from dateutil import parser


def get_data():
    # return get_minneapolis()
    return yearly_minneapolis()


def get_minneapolis():
    reader = csv.DictReader(open('temperature.csv'))
    dicts = []
    num = 0
    for line in reader:
        dicts.append(line)

    nums = []
    count = 0
    for dic in dicts:
        try:
            num = float(dic['Minneapolis'])
            nums.append(kelvin_to_f(num))

        except:
            return nums

    return nums


def yearly_minneapolis():
    reader = csv.DictReader(open('temperature.csv'))
    dicts = []
    num = 0
    for line in reader:
        dicts.append(line)

    years_temps = []
    nums = []
    year = 0
    count = 0
    for dic in dicts:
        try:
            date = parser.parse(dic['datetime'])

            if year == 0:
                year = date.year

            elif year != date.year:
                years_temps.append((nums, year))
                nums = []
                print(year)
                year = date.year

            num = float(dic['Minneapolis'])
            # if date.month == 4:
            nums.append(kelvin_to_f(num))


        except:
            nums.append(150)

    print(date)
    years_temps.append((nums, year))
    return years_temps


def kelvin_to_f(num):
    return (num - 273.15) * 1.8 + 32


def show_data(data):

    legends = []
    for temps in data:
        plot(temps[0])
        legends.append(temps[1])


    ylabel('temperature')
    xlabel('months')
    legend(legends)
    show()


data = get_data()
show_data(data)
